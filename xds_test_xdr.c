/*
**  OSSP xds - Extensible Data Serialization
**  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
**
**  This file is part of OSSP xds, an extensible data serialization
**  library which can be found at http://www.ossp.org/pkg/lib/xds/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  xds_test_xdr.c: test suite for XDR encodig/decoding engine
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <rpc/types.h>
#include <rpc/xdr.h>

#include "xds.h"

#ifdef XDS_TEST_XDR_INT32

int main(int argc, char *argv[])
{
    XDR xdrs;
    char xdr_buf[1024];
    size_t xdr_buf_size;

    xds_t *xds;
    char *xds_buf;
    size_t xds_buf_size;

    xds_int32_t values[] = {
        0x00000000,
        0x12345678,
        -0x12345678,
        0x7bcdef01,
        -0x7bcdef01,
        0x7fffffff
    };

    size_t i;

    /* Encode the values array using the RPC-XDR implementation. */
    xdrmem_create(&xdrs, xdr_buf, sizeof (xdr_buf), XDR_ENCODE);
    for (i = 0; i < sizeof (values) / sizeof (xds_int32_t); ++i)
        xdr_int32_t(&xdrs, &values[i]);
    xdr_buf_size = xdr_getpos(&xdrs);
    xdr_destroy(&xdrs);

    /* Encode the values array using the XDS implementation. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xdr_encode_int32, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_int32_t); ++i) {
        if (xds_encode(xds, "int", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&xds_buf, &xds_buf_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    /* Both buffers must be equal now. */
    if (xdr_buf_size != xds_buf_size) {
        printf("The buffer sizes don't match: %d != %d.\n", xdr_buf_size,
               xds_buf_size);
        return 1;
    }
    if (memcmp(xds_buf, xdr_buf, xds_buf_size) != 0) {
        printf("The buffers' contents is not identical!\n");
        return 1;
    }

    /* Now we decode the values again using the XDS implementation and
       compare them to our original values. Obviously, they should not
       differ. */
    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xdr_decode_int32, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, xds_buf, xds_buf_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_int32_t); ++i) {
        xds_int32_t tmp;
        if (xds_decode(xds, "int", &tmp) != XDS_OK) {
            printf("xds_encode() failed for the %d value!\n", i);
            return 1;
        }
        if (values[i] != tmp) {
            printf("The %dth value has not been decoded correctly!\n", i);
            return 1;
        }
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XDR_INT32 */

#ifdef XDS_TEST_XDR_UINT32

#ifdef __FreeBSD__
static int xdr_uint32_t(XDR *xdrs, uint32_t *val)
{
    return xdr_u_int32_t(xdrs, val);
}
#endif

int main(int argc, char *argv[])
{
    XDR xdrs;
    char xdr_buf[1024];
    size_t xdr_buf_size;

    xds_t *xds;
    char *xds_buf;
    size_t xds_buf_size;

    xds_uint32_t values[] = {
        0x00000000,
        0x12345678,
        0xabcdef01,
        0xc500b3ef,
        0xffffffff
    };

    size_t i;

    /* Encode the values array using the RPC-XDR implementation. */
    xdrmem_create(&xdrs, xdr_buf, sizeof (xdr_buf), XDR_ENCODE);
    for (i = 0; i < sizeof (values) / sizeof (xds_uint32_t); ++i)
        xdr_uint32_t(&xdrs, &values[i]);
    xdr_buf_size = xdr_getpos(&xdrs);
    xdr_destroy(&xdrs);

    /* Encode the values array using the XDS implementation. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "uint", &xdr_encode_uint32, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (uint32_t); ++i) {
        if (xds_encode(xds, "uint", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&xds_buf, &xds_buf_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    /* Both buffers must be equal now. */
    if (xdr_buf_size != xds_buf_size) {
        printf("The buffer sizes don't match: %d != %d.\n", xdr_buf_size,
               xds_buf_size);
        return 1;
    }
    if (memcmp(xds_buf, xdr_buf, xds_buf_size) != 0) {
        printf("The buffers' contents is not identical!\n");
        return 1;
    }

    /* Now we decode the values again using the XDS implementation and
       compare them to our original values. Obviously, they should not
       differ. */
    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "uint", &xdr_decode_uint32, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, xds_buf, xds_buf_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_uint32_t); ++i) {
        xds_uint32_t tmp;
        if (xds_decode(xds, "uint", &tmp) != XDS_OK) {
            printf("xds_encode() failed for the %d value!\n", i);
            return 1;
        }
        if (values[i] != tmp) {
            printf("The %dth value has not been decoded correctly!\n", i);
            return 1;
        }
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XDR_UINT32 */

#ifdef XDS_TEST_XDR_INT64

#ifdef XDS_HAVE_64_BIT_SUPPORT

int main(int argc, char *argv[])
{
    XDR xdrs;
    char xdr_buf[1024];
    size_t xdr_buf_size;

    xds_t *xds;
    char *xds_buf;
    size_t xds_buf_size;

    xds_int64_t values[] = {
        0x0000000000000000LL,
        0x123456789abcdef0LL,
        -0x123456789abcdef0LL,
        0x7bcdef01cc45bb9aLL,
        -0x7bcdef01cc45bb9aLL,
        0x7fffffffffffffffLL
    };

    size_t i;

    /* Encode the values array using the RPC-XDR implementation. */
    xdrmem_create(&xdrs, xdr_buf, sizeof (xdr_buf), XDR_ENCODE);
    for (i = 0; i < sizeof (values) / sizeof (xds_int64_t); ++i)
        xdr_int64_t(&xdrs, &values[i]);
    xdr_buf_size = xdr_getpos(&xdrs);
    xdr_destroy(&xdrs);

    /* Encode the values array using the XDS implementation. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xdr_encode_int64, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_int64_t); ++i) {
        if (xds_encode(xds, "int", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&xds_buf, &xds_buf_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    /* Both buffers must be equal now. */
    if (xdr_buf_size != xds_buf_size) {
        printf("The buffer sizes don't match: %d != %d.\n", xdr_buf_size,
               xds_buf_size);
        return 1;
    }
    if (memcmp(xds_buf, xdr_buf, xds_buf_size) != 0) {
        printf("The buffers' contents is not identical!\n");
        return 1;
    }

    /* Now we decode the values again using the XDS implementation and
       compare them to our original values. Obviously, they should not
       differ. */
    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xdr_decode_int64, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, xds_buf, xds_buf_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_int64_t); ++i) {
        xds_int64_t tmp;
        if (xds_decode(xds, "int", &tmp) != XDS_OK) {
            printf("xds_encode() failed for the %d value!\n", i);
            return 1;
        }
        if (values[i] != tmp) {
            printf("The %dth value has not been decoded correctly!\n", i);
            return 1;
        }
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_HAVE_64_BIT_SUPPORT */

#endif /* XDS_TEST_XDR_INT64 */

#ifdef XDS_TEST_XDR_UINT64

#ifdef XDS_HAVE_64_BIT_SUPPORT

#ifdef __FreeBSD__
static int xdr_uint64_t(XDR *xdrs, uint64_t *val)
{
    return xdr_u_int64_t(xdrs, val);
}
#endif

int main(int argc, char *argv[])
{
    XDR xdrs;
    char xdr_buf[1024];
    size_t xdr_buf_size;

    xds_t *xds;
    char *xds_buf;
    size_t xds_buf_size;

    xds_uint64_t values[] = {
        0x0000000000000000ULL,
        0x123456789abcdef0ULL,
        0xabcdef01cc45bb9aULL,
        0xc500b3efdd34ca9eULL,
        0xffffffffffffffffULL
    };

    size_t i;

    /* Encode the values array using the RPC-XDR implementation. */
    xdrmem_create(&xdrs, xdr_buf, sizeof (xdr_buf), XDR_ENCODE);
    for (i = 0; i < sizeof (values) / sizeof (xds_uint64_t); ++i)
        xdr_uint64_t(&xdrs, &values[i]);
    xdr_buf_size = xdr_getpos(&xdrs);
    xdr_destroy(&xdrs);

    /* Encode the values array using the XDS implementation. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "uint", &xdr_encode_uint64, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (uint64_t); ++i) {
        if (xds_encode(xds, "uint", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&xds_buf, &xds_buf_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    /* Both buffers must be equal now. */
    if (xdr_buf_size != xds_buf_size) {
        printf("The buffer sizes don't match: %d != %d.\n", xdr_buf_size,
               xds_buf_size);
        return 1;
    }
    if (memcmp(xds_buf, xdr_buf, xds_buf_size) != 0) {
        printf("The buffers' contents is not identical!\n");
        return 1;
    }

    /* Now we decode the values again using the XDS implementation and
       compare them to our original values. Obviously, they should not
       differ. */
    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "uint", &xdr_decode_uint64, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, xds_buf, xds_buf_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_uint64_t); ++i) {
        xds_uint64_t tmp;
        if (xds_decode(xds, "uint", &tmp) != XDS_OK) {
            printf("xds_encode() failed for the %d value!\n", i);
            return 1;
        }
        if (values[i] != tmp) {
            printf("The %dth value has not been decoded correctly!\n", i);
            return 1;
        }
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_HAVE_64_BIT_SUPPORT */

#endif /* XDS_TEST_XDR_UINT64 */

#ifdef XDS_TEST_XDR_STRING

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    char msg[] = "Hello World";
    char *new_msg;

    /* Encode the string as octet stream. Then erase the buffer and decode
       the string back, verifying that it hasn't changed. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "string", &xdr_encode_string, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "string", msg) != XDS_OK) {
        printf("xds_encode() failed.\n");
        return 1;
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("xds_getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);
    if (buffer_size % 4 != 0) {
        printf("The encoded strings' buffer size is not a multiple of 4.\n");
        return 1;
    }

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "string", &xdr_decode_string, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("xds_setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "string", &new_msg) != XDS_OK) {
        printf("xds_decode() failed.\n");
        return 1;
    }
    if (strlen(new_msg) != sizeof (msg) - 1) {
        printf("The size of the decoded message is wrong.\n");
        return 1;
    }
    if (memcmp(msg, new_msg, sizeof (msg) - 1) != 0) {
        printf("The decoded string is not correct.\n");
        return 1;
    }
    xds_destroy(xds);
    free(new_msg);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XDR_STRING */

#ifdef XDS_TEST_XDR_STRING_EMPTY

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    char msg[] = "";
    char *new_msg;

    /* Encode the string as octet stream. Then erase the buffer and decode
       the string back, verifying that it hasn't changed. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "string", &xdr_encode_string, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "string", msg, 0) != XDS_OK) {
        printf("xds_encode() failed.\n");
        return 1;
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("xds_getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);
    if (buffer_size % 4 != 0) {
        printf("The encoded strings' buffer size is not a multiple of 4.\n");
        return 1;
    }

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "string", &xdr_decode_string, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("xds_setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "string", &new_msg) != XDS_OK) {
        printf("xds_decode() failed.\n");
        return 1;
    }
    if (strlen(new_msg) != 0) {
        printf("The size of the decoded message is wrong.\n");
        return 1;
    }
    xds_destroy(xds);
    free(new_msg);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XDR_STRING_EMPTY */

#ifdef XDS_TEST_XDR_OCTETSTREAM

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    char msg[] = "Hello\000World";
    char *new_msg;
    size_t new_msg_size;

    /* Encode the string as octet stream. Then erase the buffer and decode
       the string back, verifying that it hasn't changed. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "os", &xdr_encode_octetstream, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "os", msg, sizeof (msg) - 1) != XDS_OK) {
        printf("xds_encode() failed.\n");
        return 1;
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("xds_getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);
    if (buffer_size % 4 != 0) {
        printf
            ("The encoded octet stream's buffer size is not a multiple of 4.\n");
        return 1;
    }

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "os", &xdr_decode_octetstream, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("xds_setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "os", &new_msg, &new_msg_size) != XDS_OK) {
        printf("xds_decode() failed.\n");
        return 1;
    }
    if (new_msg_size != sizeof (msg) - 1) {
        printf("The size of the decoded message is wrong.\n");
        return 1;
    }
    if (memcmp(msg, new_msg, new_msg_size) != 0) {
        printf("The decoded octet stream is not correct.\n");
        return 1;
    }
    xds_destroy(xds);
    free(new_msg);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XDR_OCTETSTREAM */

#ifdef XDS_TEST_XDR_OCTETSTREAM_EMPTY

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    char msg[] = "";
    char *new_msg;
    size_t new_msg_size;

    /* Encode the string as octet stream. Then erase the buffer and decode
       the string back, verifying that it hasn't changed. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "os", &xdr_encode_octetstream, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "os", msg, 0) != XDS_OK) {
        printf("xds_encode() failed.\n");
        return 1;
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("xds_getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);
    if (buffer_size % 4 != 0) {
        printf("The encoded octet stream's buffer size is not a multiple of 4.\n");
        return 1;
    }

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "os", &xdr_decode_octetstream, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("xds_setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "os", &new_msg, &new_msg_size) != XDS_OK) {
        printf("xds_decode() failed.\n");
        return 1;
    }
    if (new_msg_size != 0) {
        printf("The size of the decoded message is wrong.\n");
        return 1;
    }
    if (memcmp(msg, new_msg, new_msg_size) != 0) {
        printf("The decoded octet stream is not correct.\n");
        return 1;
    }
    xds_destroy(xds);
    free(new_msg);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XDR_OCTETSTREAM_EMPTY */

#ifdef XDS_TEST_XDR_FLOAT

int main(int argc, char *argv[])
{
    XDR xdrs;
    char xdr_buf[1024];
    size_t xdr_buf_size;

    xds_t *xds;
    char *xds_buf;
    size_t xds_buf_size;

    float values[] =
	{
        0,
	3.14,
	-3.14,
	0.14,
	-0.14,
	123456.789,
	-123456.789
	};

    size_t i;

    /* Encode the values array using the RPC-XDR implementation. */
    xdrmem_create(&xdrs, xdr_buf, sizeof (xdr_buf), XDR_ENCODE);
    for (i = 0; i < sizeof (values) / sizeof (float); ++i)
        xdr_float(&xdrs, &values[i]);
    xdr_buf_size = xdr_getpos(&xdrs);
    xdr_destroy(&xdrs);

    /* Encode the values array using the XDS implementation. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "float", &xdr_encode_float, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (float); ++i) {
        if (xds_encode(xds, "float", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&xds_buf, &xds_buf_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    /* Both buffers must be equal now. */
    if (xdr_buf_size != xds_buf_size) {
        printf("The buffer sizes don't match: %d != %d.\n", xdr_buf_size,
               xds_buf_size);
        return 1;
    }
    if (memcmp(xds_buf, xdr_buf, xds_buf_size) != 0) {
        printf("The buffers' contents is not identical!\n");
        return 1;
    }

    /* Now we decode the values again using the XDS implementation and
       compare them to our original values. Obviously, they should not
       differ. */
    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "float", &xdr_decode_float, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, xds_buf, xds_buf_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (float); ++i) {
        float tmp;
        if (xds_decode(xds, "float", &tmp) != XDS_OK) {
            printf("xds_decode() failed for the %d value!\n", i);
            return 1;
        }
        if (values[i] != tmp) {
            printf("The %dth value has not been decoded correctly!\n", i);
            return 1;
        }
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XDR_FLOAT */

#ifdef XDS_TEST_XDR_DOUBLE

int main(int argc, char *argv[])
{
    XDR xdrs;
    char xdr_buf[1024];
    size_t xdr_buf_size;

    xds_t *xds;
    char *xds_buf;
    size_t xds_buf_size;

    double values[] =
	{
        0,
	3.141592653,
	-3.14159265,
	0.14,
	-0.14,
	123456.789,
	-123456.789
	};

    size_t i;

    /* Encode the values array using the RPC-XDR implementation. */
    xdrmem_create(&xdrs, xdr_buf, sizeof (xdr_buf), XDR_ENCODE);
    for (i = 0; i < sizeof (values) / sizeof (double); ++i)
        xdr_double(&xdrs, &values[i]);
    xdr_buf_size = xdr_getpos(&xdrs);
    xdr_destroy(&xdrs);

    /* Encode the values array using the XDS implementation. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "double", &xdr_encode_double, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (double); ++i) {
        if (xds_encode(xds, "double", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&xds_buf, &xds_buf_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    /* Both buffers must be equal now. */
    if (xdr_buf_size != xds_buf_size) {
        printf("The buffer sizes don't match: %d != %d.\n", xdr_buf_size,
               xds_buf_size);
        return 1;
    }
    if (memcmp(xds_buf, xdr_buf, xds_buf_size) != 0) {
        printf("The buffers' contents is not identical!\n");
        return 1;
    }

    /* Now we decode the values again using the XDS implementation and
       compare them to our original values. Obviously, they should not
       differ. */
    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "double", &xdr_decode_double, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, xds_buf, xds_buf_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (double); ++i) {
        double tmp;
        if (xds_decode(xds, "double", &tmp) != XDS_OK) {
            printf("xds_decode() failed for the %d value!\n", i);
            return 1;
        }
        if (values[i] != tmp) {
            printf("The %dth value has not been decoded correctly!\n", i);
            return 1;
        }
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XDR_DOUBLE */
