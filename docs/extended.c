#include <stdio.h>
#include <string.h>
#include <xds.h>

struct mystruct
    {
    xds_int32_t   small;
    xds_int64_t   big;
    xds_uint32_t  positive;
    char          text[16];
    };

static int encode_mystruct(xds_t* xds, void* engine_context,
			   void* buffer, size_t buffer_size, size_t* used_buffer_size,
			   va_list* args)
    {
    struct mystruct* ms;
    ms = va_arg(*args, struct mystruct*);
    return xds_encode(xds, "int32 int64 uint32 octetstream",
		      ms->small, ms->big, ms->positive,
		      ms->text, sizeof(ms->text));
    }

static int decode_mystruct(xds_t* xds, void* engine_context,
			   void* buffer, size_t buffer_size, size_t* used_buffer_size,
			   va_list* args)
    {
    struct mystruct* ms;
    size_t i;
    char*  tmp;
    int rc;
    ms = va_arg(*args, struct mystruct*);
    rc = xds_decode(xds, "int32 int64 uint32 octetstream",
		    &(ms->small), &(ms->big), &(ms->positive),
		    &tmp, &i);
    if (rc == XDS_OK)
	{
	if (i == sizeof(ms->text))
	    memmove(ms->text, tmp, i);
	else
	    rc = XDS_ERR_TYPE_MISMATCH;
	free(tmp);
	}
    return rc;
    }

static void error_exit(int rc, const char* msg, ...)
    {
    va_list args;
    va_start(args, msg);
    vfprintf(stderr, msg, args);
    va_end(args);
    exit(rc);
    }

int main()
    {
    xds_t* xds;
    char*  buffer;
    size_t buffer_size;

    struct mystruct ms, new_ms;
    ms.small    = -0x1234567;
    ms.big      = -0x1234567890abcdeLL;
    ms.positive = 42;
    strcpy(ms.text, "Hallo World");

    /* Encode our copy of mystruct using our encoding callback. */

    xds = xds_init(XDS_ENCODE);
    if (xds == NULL)
	error_exit(1, "Failed to initialize XDS context.\n");

    if (xds_register(xds, "mystruct", &encode_mystruct, NULL) != XDS_OK ||
	xds_register(xds, "int32", &xdr_encode_int32, NULL) != XDS_OK ||
	xds_register(xds, "int64", &xdr_encode_int64, NULL) != XDS_OK ||
	xds_register(xds, "octetstream", &xdr_encode_octetstream, NULL) != XDS_OK ||
	xds_register(xds, "uint32", &xdr_encode_uint32, NULL) != XDS_OK)
	{
	error_exit(1, "Failed to register my encoding engines.\n");
	}

    if (xds_encode(xds, "mystruct", &ms) != XDS_OK)
	error_exit(1, "xds_encode() failed!\n");

    if (xds_getbuffer(xds, XDS_GIFT, (void**)&buffer, &buffer_size) != XDS_OK)
	error_exit(1, "xds_getbuffer() failed!\n");

    xds_destroy(xds);

    /* Now create a decoding context and decode the whole thing again. */

    xds = xds_init(XDS_DECODE);
    if (xds == NULL)
	error_exit(1, "Failed to initialize XDS context.\n");

    if (xds_register(xds, "mystruct", &decode_mystruct, NULL) != XDS_OK ||
	xds_register(xds, "int32", &xdr_decode_int32, NULL) != XDS_OK ||
	xds_register(xds, "int64", &xdr_decode_int64, NULL) != XDS_OK ||
	xds_register(xds, "uint32", &xdr_decode_uint32, NULL) != XDS_OK ||
	xds_register(xds, "octetstream", &xdr_decode_octetstream, NULL) != XDS_OK)
	{
	error_exit(1, "Failed to register my decoding engines.\n");
	}

    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK)
	error_exit(1, "xds_setbuffer() failed!\n");

    if (xds_decode(xds, "mystruct", &new_ms) != XDS_OK)
	error_exit(1, "xds_decode() failed!\n");

    xds_destroy(xds);

    /* Both structures must be identical. */

    if (ms.small != new_ms.small || ms.big != new_ms.big ||
	ms.positive != new_ms.positive ||
	memcmp(&ms.text, &new_ms.text, sizeof(ms.text)) != 0)
	{
	error_exit(1, "Decoded data does not match the original!\n");
	}

    /* Everything went fine. */

    return 0;
    }
