/*
**  OSSP xds - Extensible Data Serialization
**  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
**
**  This file is part of OSSP xds, an extensible data serialization
**  library which can be found at http://www.ossp.org/pkg/lib/xds/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  xds_test_xml.c: test suite for XML encodig/decoding engine
*/

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "xds_p.h"

#ifdef XDS_TEST_XML_INT32

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    xds_int32_t values[] = {
        0x00000000,
        0x12345678,
        -0x12345678,
        0x7bcdef01,
        -0x7bcdef01,
        0x7fffffff
    };

    size_t i;

    /* Encode the values array, then decode it back and compare the numbers. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "begin", &xml_encode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_encode_end, NULL) != XDS_OK ||
        xds_register(xds, "int", &xml_encode_int32, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "begin") != XDS_OK) {
        printf("xds_encode_begin() failed!\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_int32_t); ++i) {
        if (xds_encode(xds, "int", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_encode(xds, "end") != XDS_OK) {
        printf("xds_encode_end() failed!\n");
        return 1;
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "begin", &xml_decode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_decode_end, NULL) != XDS_OK ||
        xds_register(xds, "int", &xml_decode_int32, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "begin") != XDS_OK) {
        printf("xds_decode_begin() failed!\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_int32_t); ++i) {
        xds_int32_t val;
        if (xds_decode(xds, "int", &val) != XDS_OK) {
            printf("xds_decode(values[%d]) failed!\n", i);
            return 1;
        }
        if (val != values[i]) {
            printf
                ("Decoded value (%d) does not match the original value (%d)!\n",
                 val, values[i]);
            return 1;
        }
    }
    if (xds_decode(xds, "end") != XDS_OK) {
        printf("xds_decode_end() failed!\n");
        return 1;
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XML_INT32 */

#ifdef XDS_TEST_XML_UINT32

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    xds_uint32_t values[] = {
        0x00000000,
        0x12345678,
        0xabcdef01,
        0xc500b3ef,
        0xffffffff
    };

    size_t i;

    /* Encode the values array, then decode it back and compare the numbers. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xml_encode_uint32, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_uint32_t); ++i) {
        if (xds_encode(xds, "int", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xml_decode_uint32, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_uint32_t); ++i) {
        xds_uint32_t val;
        if (xds_decode(xds, "int", &val) != XDS_OK) {
            printf("xds_decode(values[%d]) failed!\n", i);
            return 1;
        }
        if (val != values[i]) {
            printf
                ("Decoded value (%d) does not match the original value (%d)!\n",
                 val, values[i]);
            return 1;
        }
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XML_UINT32 */

#ifdef XDS_TEST_XML_INT64

#ifdef XDS_HAVE_64_BIT_SUPPORT

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    xds_int64_t values[] = {
        0x0000000000000000LL,
        0x123456789abcdef0LL,
        -0x123456789abcdef0LL,
        0x7bcdef01cc45bb9aLL,
        -0x7bcdef01cc45bb9aLL,
        0x7fffffffffffffffLL
    };

    size_t i;

    /* Encode the values array, then decode it back and compare the numbers. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xml_encode_int64, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_int64_t); ++i) {
        if (xds_encode(xds, "int", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xml_decode_int64, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_int64_t); ++i) {
        xds_int64_t val;
        if (xds_decode(xds, "int", &val) != XDS_OK) {
            printf("xds_decode(values[%d]) failed!\n", i);
            return 1;
        }
        if (val != values[i]) {
            printf("Decoded value does not match the original!\n");
            return 1;
        }
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_HAVE_64_BIT_SUPPORT */

#endif /* XDS_TEST_XML_INT64 */

#ifdef XDS_TEST_XML_UINT64

#ifdef XDS_HAVE_64_BIT_SUPPORT

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    xds_uint64_t values[] = {
        0x0000000000000000ULL,
        0x123456789abcdef0ULL,
        0xabcdef01cc45bb9aULL,
        0xc500b3efdd34ca9eULL,
        0xffffffffffffffffULL
    };

    size_t i;

    /* Encode the values array, then decode it back and compare the numbers. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xml_encode_uint64, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_uint64_t); ++i) {
        if (xds_encode(xds, "int", values[i]) != XDS_OK) {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
        }
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "int", &xml_decode_uint64, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    for (i = 0; i < sizeof (values) / sizeof (xds_uint64_t); ++i) {
        xds_uint64_t val;
        if (xds_decode(xds, "int", &val) != XDS_OK) {
            printf("xds_decode(values[%d]) failed!\n", i);
            return 1;
        }
        if (val != values[i]) {
            printf("Decoded value does not match the original!\n");
            return 1;
        }
    }
    xds_destroy(xds);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_HAVE_64_BIT_SUPPORT */

#endif /* XDS_TEST_XML_UINT64 */

#ifdef XDS_TEST_XML_DOUBLE

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    xds_double_t values[] = {
        0.0,
        -0.0,
        3.141592,
        -3.141592,
        298473.141592,
        -298473.141592
    };

    size_t i;

    /* Encode the values array, then decode it back and compare the numbers. */

    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "begin", &xml_encode_begin, NULL) != XDS_OK ||
	xds_register(xds, "end", &xml_encode_end, NULL) != XDS_OK ||
	xds_register(xds, "double", &xml_encode_double, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "begin") != XDS_OK)
	{
	printf("xds_encode_begin() failed!\n");
	return 1;
	}
    for (i = 0; i < sizeof (values) / sizeof (xds_double_t); ++i)
	{
        if (xds_encode(xds, "double", values[i]) != XDS_OK)
	    {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
	    }
	}
    if (xds_encode(xds, "end") != XDS_OK)
	{
	printf("xds_encode_end() failed!\n");
	return 1;
	}
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) != XDS_OK)
	{
        printf("getbuffer() failed.\n");
        return 1;
	}
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "begin", &xml_decode_begin, NULL) != XDS_OK ||
	xds_register(xds, "end", &xml_decode_end, NULL) != XDS_OK ||
	xds_register(xds, "double", &xml_decode_double, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "begin") != XDS_OK)
	{
	printf("xds_decode_begin() failed!\n");
	return 1;
	}
    for (i = 0; i < sizeof (values) / sizeof (xds_double_t); ++i) {
        xds_double_t val;
        if (xds_decode(xds, "double", &val) != XDS_OK) {
            printf("xds_decode(values[%d]) failed!\n", i);
            return 1;
        }
        if (val != values[i]) {
	    printf("Decoding the %dth value failed!\n", i);
            return 1;
        }
    }
    if (xds_decode(xds, "end") != XDS_OK)
	{
	printf("xds_decode_end() failed!\n");
	return 1;
	}
    xds_destroy(xds);

    /* Everything went fine. */

    return 0;
}

#endif /* XDS_TEST_XML_DOUBLE */

#ifdef XDS_TEST_XML_FLOAT

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    xds_float_t values[] = {
        0.0,
        -0.0,
        3.141592,
        -3.141592,
        298473.141592,
        -298473.141592
    };

    size_t i;

    /* Encode the values array, then decode it back and compare the numbers. */

    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "begin", &xml_encode_begin, NULL) != XDS_OK ||
	xds_register(xds, "end", &xml_encode_end, NULL) != XDS_OK ||
	xds_register(xds, "float", &xml_encode_float, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "begin") != XDS_OK)
	{
	printf("xds_encode_begin() failed!\n");
	return 1;
	}
    for (i = 0; i < sizeof (values) / sizeof (xds_float_t); ++i)
	{
        if (xds_encode(xds, "float", values[i]) != XDS_OK)
	    {
            printf("xds_encode(values[%d]) failed!\n", i);
            return 1;
	    }
	}
    if (xds_encode(xds, "end") != XDS_OK)
	{
	printf("xds_encode_end() failed!\n");
	return 1;
	}
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) != XDS_OK)
	{
        printf("getbuffer() failed.\n");
        return 1;
	}
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "begin", &xml_decode_begin, NULL) != XDS_OK ||
	xds_register(xds, "end", &xml_decode_end, NULL) != XDS_OK ||
	xds_register(xds, "float", &xml_decode_float, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "begin") != XDS_OK)
	{
	printf("xds_decode_begin() failed!\n");
	return 1;
	}
    for (i = 0; i < sizeof (values) / sizeof (xds_float_t); ++i) {
        xds_float_t val;
        if (xds_decode(xds, "float", &val) != XDS_OK) {
            printf("xds_decode(values[%d]) failed!\n", i);
            return 1;
        }
        if (val != values[i]) {
	    printf("Decoding the %dth value failed!\n", i);
            return 1;
        }
    }
    if (xds_decode(xds, "end") != XDS_OK)
	{
	printf("xds_decode_end() failed!\n");
	return 1;
	}
    xds_destroy(xds);

    /* Everything went fine. */

    return 0;
}

#endif /* XDS_TEST_XML_FLOAT */

#ifdef XDS_TEST_XML_STRING

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    char msg[] = "<Hallo> ]]>&<&>World: ����";
    char *new_msg;

    /* Encode the string, then erase the buffer and decode the string back,
       verifying that it hasn't changed. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "string", &xml_encode_string, NULL) != XDS_OK ||
        xds_register(xds, "begin", &xml_encode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_encode_end, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "begin string end", msg, sizeof (msg) - 1) != XDS_OK) {
        printf("xds_encode() failed.\n");
        return 1;
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("xds_getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "string", &xml_decode_string, NULL) != XDS_OK ||
        xds_register(xds, "begin", &xml_decode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_decode_end, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("xds_setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "begin string end", &new_msg) != XDS_OK) {
        printf("xds_decode() failed.\n");
        return 1;
    }
    if (strlen(new_msg) != sizeof (msg) - 1) {
        printf("The size of the decoded message is wrong.\n");
        return 1;
    }
    if (memcmp(msg, new_msg, sizeof (msg) - 1) != 0) {
        printf("The decoded string is not correct.\n");
        return 1;
    }
    xds_destroy(xds);
    free(new_msg);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XML_STRING */

#ifdef XDS_TEST_XML_STRING_EMPTY

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    char msg[] = "";
    char *new_msg;

    /* Encode the string, then erase the buffer and decode the string back,
       verifying that it hasn't changed. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "string", &xml_encode_string, NULL) != XDS_OK ||
        xds_register(xds, "begin", &xml_encode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_encode_end, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "begin string end", msg, sizeof (msg) - 1) != XDS_OK) {
        printf("xds_encode() failed.\n");
        return 1;
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("xds_getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "string", &xml_decode_string, NULL) != XDS_OK ||
        xds_register(xds, "begin", &xml_decode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_decode_end, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("xds_setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "begin string end", &new_msg) != XDS_OK) {
        printf("xds_decode() failed.\n");
        return 1;
    }
    if (strlen(new_msg) != sizeof (msg) - 1) {
        printf("The size of the decoded message is wrong.\n");
        return 1;
    }
    if (memcmp(msg, new_msg, sizeof (msg) - 1) != 0) {
        printf("The decoded string is not correct.\n");
        return 1;
    }
    xds_destroy(xds);
    free(new_msg);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XML_STRING_EMPTY */

#ifdef XDS_TEST_XML_OCTETSTREAM

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    char msg[] = "Hallo\000Worl";
    char *new_msg;
    size_t new_msg_size;

    /* Encode the string as octet stream. Then erase the buffer and decode
       the string back, verifying that it hasn't changed. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "os", &xml_encode_octetstream, NULL) != XDS_OK ||
        xds_register(xds, "begin", &xml_encode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_encode_end, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "begin os end", msg, sizeof (msg) - 1) != XDS_OK) {
        printf("xds_encode() failed.\n");
        return 1;
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("xds_getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "os", &xml_decode_octetstream, NULL) != XDS_OK ||
        xds_register(xds, "begin", &xml_decode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_decode_end, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("xds_setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "begin os end", &new_msg, &new_msg_size) != XDS_OK) {
        printf("xds_decode() failed.\n");
        return 1;
    }
    if (new_msg_size != sizeof (msg) - 1) {
        printf("The size of the decoded message is wrong.\n");
        return 1;
    }
    if (memcmp(msg, new_msg, new_msg_size) != 0) {
        printf("The decoded octet stream is not correct.\n");
        return 1;
    }
    xds_destroy(xds);
    free(new_msg);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XML_OCTETSTREAM */

#ifdef XDS_TEST_XML_OCTETSTREAM_EMPTY

int main(int argc, char *argv[])
{
    xds_t *xds;
    char *buffer;
    size_t buffer_size;

    char msg[] = "";
    char *new_msg;
    size_t new_msg_size;

    /* Encode the string as octet stream. Then erase the buffer and decode
       the string back, verifying that it hasn't changed. */
    if (xds_init(&xds, XDS_ENCODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "os", &xml_encode_octetstream, NULL) != XDS_OK ||
        xds_register(xds, "begin", &xml_encode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_encode_end, NULL) != XDS_OK) {
        printf("Failed to register my encoding engines.\n");
        return 1;
    }
    if (xds_encode(xds, "begin os end", msg, sizeof (msg) - 1) != XDS_OK) {
        printf("xds_encode() failed.\n");
        return 1;
    }
    if (xds_getbuffer(xds, XDS_GIFT, (void **)&buffer, &buffer_size) !=
        XDS_OK) {
        printf("xds_getbuffer() failed.\n");
        return 1;
    }
    xds_destroy(xds);

    if (xds_init(&xds, XDS_DECODE) != XDS_OK) {
        printf("Failed to initialize XDS context.\n");
        return 1;
    }
    if (xds_register(xds, "os", &xml_decode_octetstream, NULL) != XDS_OK ||
        xds_register(xds, "begin", &xml_decode_begin, NULL) != XDS_OK ||
        xds_register(xds, "end", &xml_decode_end, NULL) != XDS_OK) {
        printf("Failed to register my decoding engines.\n");
        return 1;
    }
    if (xds_setbuffer(xds, XDS_GIFT, buffer, buffer_size) != XDS_OK) {
        printf("xds_setbuffer() failed.\n");
        return 1;
    }
    if (xds_decode(xds, "begin os end", &new_msg, &new_msg_size) != XDS_OK) {
        printf("xds_decode() failed.\n");
        return 1;
    }
    if (new_msg_size != sizeof (msg) - 1) {
        printf("The size of the decoded message is wrong.\n");
        return 1;
    }
    if (memcmp(msg, new_msg, new_msg_size) != 0) {
        printf("The decoded octet stream is not correct.\n");
        return 1;
    }
    xds_destroy(xds);
    free(new_msg);

    /* Everything went fine. */
    return 0;
}

#endif /* XDS_TEST_XML_OCTETSTREAM_EMPTY */
