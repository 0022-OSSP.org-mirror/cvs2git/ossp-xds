dnl ##
dnl ##  OSSP xds - Extensible Data Serialization
dnl ##  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
dnl ##  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
dnl ##  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
dnl ##
dnl ##  This file is part of OSSP xds, an extensible data serialization
dnl ##  library which can be found at http://www.ossp.org/pkg/lib/xds/.
dnl ##
dnl ##  Permission to use, copy, modify, and distribute this software for
dnl ##  any purpose with or without fee is hereby granted, provided that
dnl ##  the above copyright notice and this permission notice appear in all
dnl ##  copies.
dnl ##
dnl ##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
dnl ##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
dnl ##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
dnl ##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
dnl ##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
dnl ##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
dnl ##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
dnl ##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
dnl ##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
dnl ##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
dnl ##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
dnl ##  SUCH DAMAGE.
dnl ##
dnl ##  aclocal.m4: local autoconf macros
dnl ##

dnl ##
dnl ##  Support for Configuration Headers
dnl ##
dnl ##  configure.in:
dnl ##    AC_HEADLINE(<short-name>, <long-name>,
dnl ##                <vers-var>, <vers-file>,
dnl ##                <copyright>)
dnl ##

AC_DEFUN(AC_HEADLINE,[dnl
#   configuration header
if test ".`echo dummy [$]@ | grep help`" = .; then
    #   bootstrapping shtool
    ac_prog=[$]0
changequote(, )dnl
    ac_srcdir=`echo $ac_prog | sed -e 's%/[^/][^/]*$%%' -e 's%\([^/]\)/*$%\1%'`
changequote([, ])dnl
    test ".$ac_srcdir" = ".$ac_prog" && ac_srcdir=.
    ac_shtool="$ac_srcdir/shtool"

    #   find out terminal sequences
    TB=`$ac_shtool echo -n -e %B 2>/dev/null`
    TN=`$ac_shtool echo -n -e %b 2>/dev/null`

    #   find out package version
    $3_STR="`$ac_shtool version -l c -d long $ac_srcdir/$4`"
    AC_SUBST($3_STR)

    #   friendly header ;)
    echo "Configuring ${TB}$1${TN} ($2), Version ${TB}${$3_STR}${TN}"
    echo "$5"

    #   additionally find out hex version
    $3_HEX="`$ac_shtool version -l c -d hex $ac_srcdir/$4`"
    AC_SUBST($3_HEX)
fi
])dnl

dnl ##
dnl ##  Display Configuration Headers
dnl ##
dnl ##  configure.in:
dnl ##    AC_MSG_PART(<text>)
dnl ##

define(AC_MSG_PART,[dnl
if test ".$enable_subdir" != .yes; then
    AC_MSG_RESULT()
    AC_MSG_RESULT(${TB}$1:${TN})
fi
])dnl

dnl ##
dnl ##  Support for Platform IDs
dnl ##
dnl ##  configure.in:
dnl ##    AC_PLATFORM(<variable>)
dnl ##

AC_DEFUN(AC_PLATFORM,[
if test ".$host" != .NONE && test ".$host" != .; then
    $1="$host"
else
    $1=`${CONFIG_SHELL-/bin/sh} $srcdir/config.guess`
fi
$1=`${CONFIG_SHELL-/bin/sh} $srcdir/config.sub $$1` || exit 1
AC_SUBST($1)
if test ".$enable_subdir" != .yes; then
    echo "Platform: ${TB}${$1}${TN}"
fi
])dnl

dnl ##
dnl ##  Check whether compiler option works
dnl ##
dnl ##  configure.in:
dnl ##    AC_COMPILER_OPTION(<name>, <display>, <option>,
dnl ##                       <action-success>, <action-failure>)
dnl ##

AC_DEFUN(AC_COMPILER_OPTION,[dnl
AC_MSG_CHECKING(whether compiler option(s) $2 work)
AC_CACHE_VAL(ac_cv_compiler_option_$1,[
SAVE_CFLAGS="$CFLAGS"
CFLAGS="$CFLAGS $3"
AC_TRY_COMPILE([],[], ac_cv_compiler_option_$1=yes, ac_cv_compiler_option_$1=no)
CFLAGS="$SAVE_CFLAGS"
])dnl
if test ".$ac_cv_compiler_option_$1" = .yes; then
    ifelse([$4], , :, [$4])
else
    ifelse([$5], , :, [$5])
fi
AC_MSG_RESULT([$ac_cv_compiler_option_$1])
])dnl

dnl ##
dnl ##  Debugging Support
dnl ##
dnl ##  configure.in:
dnl ##    AC_CHECK_DEBUGGING
dnl ##

AC_DEFUN(AC_CHECK_DEBUGGING,[dnl
AC_ARG_ENABLE(debug,dnl
[  --enable-debug          build for debugging (default=no)],
[dnl
if test ".$ac_cv_prog_gcc" = ".yes"; then
    case "$CFLAGS" in
        *-O* ) ;;
           * ) CFLAGS="$CFLAGS -O2" ;;
    esac
    case "$CFLAGS" in
        *-g* ) ;;
           * ) CFLAGS="$CFLAGS -g" ;;
    esac
    case "$CFLAGS" in
        *-pipe* ) ;;
              * ) AC_COMPILER_OPTION(pipe, -pipe, -pipe, CFLAGS="$CFLAGS -pipe") ;;
    esac
    CFLAGS="$CFLAGS -pedantic"
    CFLAGS="$CFLAGS -Wall"
    WMORE="-Wshadow -Wpointer-arith -Wcast-align -Winline"
    WMORE="$WMORE -Wmissing-prototypes -Wmissing-declarations -Wnested-externs"
    AC_COMPILER_OPTION(wmore, -W<xxx>, $WMORE, CFLAGS="$CFLAGS $WMORE")
    AC_COMPILER_OPTION(wnolonglong, -Wno-long-long, -Wno-long-long, CFLAGS="$CFLAGS -Wno-long-long")
else
    case "$CFLAGS" in
        *-g* ) ;;
           * ) CFLAGS="$CFLAGS -g" ;;
    esac
fi
msg="enabled"
dnl AC_DEFINE(XDS_DEBUG)
],[
if test ".$ac_cv_prog_gcc" = ".yes"; then
case "$CFLAGS" in
    *-pipe* ) ;;
          * ) AC_COMPILER_OPTION(pipe, -pipe, -pipe, CFLAGS="$CFLAGS -pipe") ;;
esac
fi
case "$CFLAGS" in
    *-g* ) CFLAGS=`echo "$CFLAGS" |\
                   sed -e 's/ -g / /g' -e 's/ -g$//' -e 's/^-g //g' -e 's/^-g$//'` ;;
esac
case "$CXXFLAGS" in
    *-g* ) CXXFLAGS=`echo "$CXXFLAGS" |\
                     sed -e 's/ -g / /g' -e 's/ -g$//' -e 's/^-g //g' -e 's/^-g$//'` ;;
esac
msg=disabled
])dnl
AC_MSG_CHECKING(for compilation debug mode)
AC_MSG_RESULT([$msg])
if test ".$msg" = .enabled; then
    enable_shared=no
fi
])

dnl ##
dnl ##  Libtool Glue Code
dnl ##
dnl ##  configure.in:
dnl ##    AC_CONFIGURE_LIBTOOL
dnl ##

AC_DEFUN(AC_CONFIGURE_LIBTOOL,[
    sinclude(libtool.m4)
    AC_PROG_LIBTOOL
])

dnl ##
dnl ##  XDS Glue Code
dnl ##
dnl ##  configure.in:
dnl ##    AC_CONFIGURE_XDS
dnl ##

AC_DEFUN(AC_CONFIGURE_XDS,[
    sinclude(xds.m4)
    AC_XDS
])

