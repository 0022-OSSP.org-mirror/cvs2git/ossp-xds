#!/bin/sh
##
##  OSSP xds - Extensible Data Serialization
##  Copyright (c) 2001-2005 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2001-2005 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2001-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP xds, an extensible data serialization
##  library which can be found at http://www.ossp.org/pkg/lib/xds/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  xds_test.sh: test suite driver script
##

run="$1"
shift

if [ $# -lt 1 ]; then
    echo "Usage: $0 <run> <test> [<test> ...]"
    exit 1
fi

RESCOLUMN=30
numTests=0
numFails=0

echo "Running test suite:"

pad=''
n=$RESCOLUMN
while [ $n -gt 0 ]; do
    pad="$pad."
    n=`expr $n - 1`
done
for suite in "$@"; do
    name=`expr "${suite}" : '\./\.\(.*\)\.t$'`
    echo "$name$pad" | awk '{ printf("%s ", substr($0, 0, n)); }' n=$RESCOLUMN
    numTests=`expr $numTests + 1`
    eval $run $suite >.${name}.l 2>&1
    if [ $? -eq 0 ]; then
        echo "OK"
    else
        numFails=`expr $numFails + 1`
        echo "FAILED"
    fi
done

echo
if [ $numFails -eq 0 ]; then
    echo "Summary: All tests succeeded."
    exit 0
else
    percent=`expr $numFails \* 100`
    percent=`expr $percent / $numTests`
    echo "Summary: $numFails of $numTests tests failed ($percent%)."
    exit 1
fi
